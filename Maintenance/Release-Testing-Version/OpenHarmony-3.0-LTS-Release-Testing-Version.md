# OpenHarmony 3.0.0.21版本转测试信息

转测试版本号：OpenHarmony 3.0.0.21
版本用途：tag标签版本
转测试时间：2021/12/29
版本获取路径：
hispark_pegasus_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_010124/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_010124-hispark_pegasus_3_0-LTS.tar.gz

hispark_taurus_LiteOS_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_030136/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_030136-hispark_taurus_LiteOS_3_0-LTS.tar.gz

hispark_aries_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_030126/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_030126-hispark_aries_3_0-LTS.tar.gz

hispark_taurus_L2_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_000048/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_000048-hispark_taurus_L2_3_0-LTS.tar.gz

hispark_taurus_Linux_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_000126/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_000126-hispark_taurus_Linux_3_0-LTS.tar.gz



# OpenHarmony 3.0.0.20版本转测试信息

转测试版本号：OpenHarmony 3.0.0.20
版本用途：内部测试
转测试时间：2021/12/17
版本获取路径：

hispark_taurus_L2_3_0-LTS（L2）
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_000122/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_000122-hispark_taurus_L2_3_0-LTS.tar.gz
hispark_pegasus_3_0-LTS（L0）：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_080128/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_080128-hispark_pegasus_3_0-LTS.tar.gz
hispark_aries_3_0-LTS（L1 3518）
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_030127/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_030127-hispark_aries_3_0-LTS.tar.gz
hispark_taurus_LiteOS_3_0-LTS（L1 3516 LiteOS） 
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_030128/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_030128-hispark_taurus_LiteOS_3_0-LTS.tar.gz
hispark_taurus_Linux_3_0-LTS (L1 3516 Linux)
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_000134/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_000134-hispark_taurus_Linux_3_0-LTS.tar.gz

L2_SDK_Mac_LTS：（L2 SDK包）
https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_3.0_LTS/20211217_004915/L2-SDK-MAC.tar.gz